# SPDX-FileCopyrightText: 2011-2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

All software archives (ZIP files) contain their own license;
all other files in this repository are licensed under Apache 2.0.

Note that some licenses of contained software archives may not allow redistribution
and restrict their usage to reproduction of the competition results.

The contributors of the software archives are contained in the file `CODEOWNERS`.

